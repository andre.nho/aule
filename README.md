# TODO
* are we going to need a heatsink? If yes, put power in main board.

# Boards

## Motherboard

* Pinage (21 pins - two rows of 12)
  * 5V, GND
  * D0-D7
  * A0-A3
  * Clock (25 Mhz, 12.5 Mhz, 6.2 Mhz)
  * IORQ, RD, WR, RESET
  * INT, NMI, BUSRQ, BUSAK, HALT will not be used (?)
* 4 ports (cpu, video, devices and extra)
* debugger is connected directly to CPU

* Power supply (inversion protection, overcurrent protection)
* Reset circuitry
* Led
* Clock generation (?) - 25 MHz, divide by 2 and 4
  * Frequence division: http://www.electronics-tutorials.ws/counter/count_1.html
  * if possible, replace for 25.175 MHz

Questions:
* have pins for ports A8-15? (this would reduce to 32 pins)  no
* debugging in the main board?  no

* inputs: none
* outputs: CLK6, CLK13, CLK25, RESET
* power (output): 5V, GND

External ports:
* 7,5V power
* power button
* power LED
* reset button


## CPU/Memory

* ROM loader (uC -> RAM)
  * ROM is stored in uC
  * Use ATTINY2313
  * When turning on, uC stores ROM in RAM, and then turn Z80 on
  * Alternative - uC reads kernel from SDCard and then loads into memory
* Connector to update uC
* Z80 - CPU at 6 MHz (or 13 Mhz? - check memory speed)
* Two memory ICs (32 kB)
* 74139 for deciding which memory to use
* IC to choose which clock to use (main or debugger)

* debugger is connected directly to CPU

Questions:
* use demuxer to decide port in use?  no
* 6 MHz or 12 Mhz? 6 for now, 12 maybe later
* add timer circuit?  yes, can use same same uC

* inputs: CLK6, CLK12, IORQ, RESET, WAIT
* input/output: D0-D7
* output: A0-A3 ?
* power: 5V, GND
* to debugger: 5V, GND, D0-D7, A0-A15, MREQ, IORQ, RD, WR, RESET, M1
* from debugger: WAIT, EXTCLK, SELCLK

## Video
* Screen: 320x480
* Font: 5x14 (18 Kb needed for ROM - can be reduced to 16 Kb if needed)
* Characters: 64x33 (3 Kb for chars, 5 Kb chars + colors)

* uC for generating HSYNC, VSYNC signals, drawing chars
  * uC select position -> ROM output serial char -> serialize to draw char (?)

* copy RAM data during VSYNC

* CPU will send commands to video through I/O communication
  * will need a buffer to store the commands while the video is drawn?
  * alterative, use some sort of RAM? maybe accessed through CPU I/O?
* ATTINY2313
* AT25C256 (character ROM)
* 74HC165 for deserialize char
* Connector to update uC
* test button (internal)

* remember reset!

* External:
  * VGA output

Example: http://fpga4fun.com/GraphicLCDpanel4.html

Questions:
* can we have 640x480?  let's try
* how about colors? maybe 4 colors?  no colors for now

* inputs: A0-A3, D0-D7, CLK25, IORQ, WR, RESET
* outputs: none
* power: 5V, GND

## SDCard + Keyboard + Audio
* uC for reading USB keyboard, possibly for reading SDCard
* uC could do the first kernel translation (FAT), to ease the bootloader job
* buffer

* remeber reset!

Questions:
  * Do we need a level change? (5V to 3.3V)
  * Bitbang SPI from Z80, or use I/O?

* inputs: A0-A3, CLK13, IORQ, RD, WR, RESET
* input/output: D0-D7
* outputs: WAIT


## Debugger
* cycle button
* step button, circuitry (or uC?)
* 7-seg output of A0-A15
* 7-seg output of D0-D7
* LEDs for other pins
* parallel to serial to read A and D, and possible LEDs
* ATTINY2313 for 7-seg
* ULN2801 for the LEDS
* clock selector (disable main clock for single stepping)

* inputs: D0-D7, A0-A15, MREQ, IORQ, RD, WR, RESET, M1
* outputs: WAIT, EXTCLK, SELCLK
* power: 5V, GND

# Order

Steps 1, 2, 3, 4 and 7 can happen in parallel.

1. ROM burner
  1. Draw schematic
  2. Write client code
  3. Write uC code
  4. Test on breadboard
  5. Build circuit
2. Control board
  1. Draw schematic
  2. Power supply
  3. Clock
  4. Reset circuitry
  5. Build circuit (depends on 8.1)
3. Video output
  1. Draw schematic
  2. Basic signals (HSYNC/VSYNC)
  3. Basic drawing
  4. Create ROM
  5. Draw basic characters
  6. Read from RAM / I/O / SPI
  7. Build circuit
4. Debugger
  1. Draw schematic
  2. Read ports (7-seg, LEDs)
  3. Step button circuitry
  4. Serial communication (?)
  5. Build circuit
5. CPU (depends on 4)
  1. Draw schematic
  2. Code uC
  3. Test basic steps
  4. Test full run
  5. Build circuit
6. SDCard + Keyboard
  1. Draw schematic
  2. Code
  3. Test
  4. Build circuit
7. Motherboard
  1. Define size
  2. Build circuit
8. Build
  1. Measure all sizes
  2. Make holes
  3. Attach everything
  4. Beautify
