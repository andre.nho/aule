/* CPU runs at 12,5875 Mhz, each cycle = 79.4 ns */


/* When the computer is turned on, this code:
 *   1. Loads static memory data into RAM. It feeds
 *       - address through a 4040 counter
 *       - data via data bus
 *   2. Goes into high impedance mode, and turns on the Z80 processor
 *   3. Resets the processor for X ms
 *
 * The ports used are:
 *   PB0~7 : data bus
 *   PD1   : ~RESET (Z80)
 *   PD2   : enable Z80
 *   PD3   : enable 4040 counter
 *   PD4   : ~MREQ (memory request)
 *   PD5   : ~WR (memory write mode)
 *   PD6   : serial clock */

#include <stdbool.h>

#define F_CPU 12587500
//#define F_CPU 12000000

#include <avr/io.h>
#include <avr/cpufunc.h>
#include <util/delay.h>

#include "bios.inc"

static void
prepare_pins()
{
    DDRB = 0xFF;
    PORTB = 0xAB;
    DDRD = 0xFF;
    DDRD &= ~(1 << PD2);  // put clock in high impedance
    PORTD = (1 << PD4) | (1 << PD5);
    PORTD &= ~(1 << PD1); // CPU reset is active
}


static void
enable_counter(bool activate)
{
    // enable
    if (activate) {
        PORTD |= (1 << PD3);
        // reset
        PORTD |= (1 << PD0);
        PORTD &= ~(1 << PD0);
    } else {
        PORTD &= ~(1 << PD3);
    }
    _delay_ms(1);
}


static void
store_data(unsigned int sz, unsigned char data[])
{
    PORTD &= ~(1 << PD4);  // MREQ active
    for (unsigned int i=0; i < sz; ++i) {
        // set DATA bus
        PORTB = data[i]; _NOP();
        // RAM writing cycle
        PORTD &= ~(1 << PD5); _NOP();
        PORTD |= (1 << PD5); _NOP();
        // advance counter cycle
        PORTD |= (1 << PD6);
        PORTD &= ~(1 << PD6);
    }
    PORTD |= (1 << PD4);  // MREQ inactive
}


static void
turn_on_z80()
{
    // go into high impedance mode
    DDRB = 0x0;
    DDRD &= ~(1 << PD4);  PORTD &= ~(1 << PD4);  // ~MREQ in high impedance
    DDRD &= ~(1 << PD5);  PORTD &= ~(1 << PD5);  // ~WR in high impedance
    DDRD &= ~(1 << PD6);  PORTD &= ~(1 << PD6);  // serial clock high impedance

    // do a few clock cycles, in case the debugger is in debug mode
    DDRD |= (1 << PD2);
    for (int i=0; i < 50; ++i) {
        PORTD |= (1 << PD2);
        _delay_us(50);
        PORTD &= ~(1 << PD2);
    }
    DDRD &= ~(1 << PD2);  // put clock in high impedance
    
    // let go of CPU reset
    PORTD |= (1 << PD1);
}


int main()
{
    prepare_pins();
    enable_counter(true);
    store_data(bios_rom_len, bios_rom);
    enable_counter(false);
    turn_on_z80();
}


// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
