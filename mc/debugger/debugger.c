/* Activities:
 *   - Detect button press (STEP and CYCLE) and debounce it
 *   - Advance one cycle when CYCLE is pressed
 *   - Advance one step when STEP is pressed
 *   - Read address and data bus lines via serial input
 *   - Refresh 7-segment displays */

/* Strategy:
 *   - An interrupt is called every 2.5 ms
 *   - The interrupt sets the next action according to the timing:
 *      - Refresh display every 1 interrupt
 *      - Detect button press every 40 interrupts
 *      - Read address and data bus every 20 interrupts (?)
 *   - A main loop will coordinate these actions, so they don't overlap.
 */

#include <stdbool.h>
#include <stdint.h>

#define F_CPU 8000000

#include <avr/cpufunc.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/power.h>
#include <util/delay.h>

// CPU runs at 8 Mhz

struct Event {
    bool readbus  : 1;
    bool btdetect : 1;
    bool refresh  : 1;
} event = { false, false, false };

static volatile uint16_t address = 0x1234;
static volatile uint8_t  data = 0x56;

// {{{ INITIALIZATION & TIMER

static void
prepare_pins()
{
    DDRB = 0;     // inputs: PB0 - serial, PB3 - STEP button, PB4 - CYCLE button, PB5 - M1
    DDRB |= ((1 << PB1) | (1 << PB2));  // outputs: PB1 - serial clock, PB2 - parallel load
    DDRC = 0xFF;  // PORTC = 7-seg anodes
    DDRD = 0xFF;  // PORTD0~6 = 7-seg leds, PORTD7 = external clock

    PORTB |= (1 << PB2);  // PB2 - parallel load active
    PORTB |= ((1 << PB3) | (1 << PB4));  // PB3 & PB4 = pull up on buttons

    // TODO - disable ADC
}

static void
prepare_timer()
{
    // timer interrupt will run at every 2.5ms or 20000 ticks without prescaler

    TCCR1B |= (1 << WGM12);     // initialize CTC (clear on time compare) mode
    OCR1A = 20000;              // run interrupt every 20000 ticks
    
    TIMSK |= (1 << OCIE1A);     // setup interrupt
    sei();

    TCCR1B |= (1 << CS10);      // initialize timer without prescaler
}


ISR(TIMER1_COMPA_vect)
{
    event.refresh = true;

    static uint32_t ctr = 0;
    if ((ctr % 20) == 0) {
        event.readbus = true;
    }

    if ((ctr % 40) == 0) {
        event.btdetect = true;
    }
    ++ctr;
}

// }}}

// {{{ ADDRESS & DATA BUS


static inline void pl(bool v) { if (v) PORTB |= (1<<PB2); else PORTB &= ~(1<<PB2); }
static inline void cp(bool v) { if (v) PORTB |= (1<<PB1); else PORTB &= ~(1<<PB1); }
static inline bool q7() { return (PINB >> PB0) & 1; }

static void
read_bus()
{
    // load data into chips
    pl(0);
    pl(1);

    // read serial data
    uint32_t value = 0;
    for (int i=0; i < 24; ++i) {
        value |= ((uint32_t)q7() << (23-i));
        cp(1);
        cp(0);
    }

    address = value & 0xFFFF;
    data = (value >> 16) & 0xFF;
}

// }}}

// {{{ BUTTONS & CPU MANAGEMENT

static void
cpu_cycle()
{
    PORTD &= ~(1 << PD7);
    _delay_ms(5);
    PORTD |= (1 << PD7);
    _delay_ms(5);
}


static void
button_detect()
{
    if (((PINB >> PB3) & 1) == 0) {  // step button pressed
        PORTC = 0;       // disable LEDs
        cpu_cycle();
        while ((PINB >> PB5) & 1) {  // advance CPU until M1 == 0 again
            cpu_cycle();
        }
        _delay_ms(200);  // debounce
    } else if (((PINB >> PB4) & 1) == 0) {  // cycle button pressed
        PORTC = 0;       // disable LEDs
        cpu_cycle();
        _delay_ms(200);  // debounce
    }
}

// }}}

// {{{ 7-SEG DISPLAY

static void
display_refresh()
{
    static uint8_t display = 0;

    static const uint8_t digits[] = { 
    // {{{
        //abcdefg
        0b0111111,  // 0
        0b0000110,  // 1
        0b1011011,  // 2
        0b1001111,  // 3
        0b1100110,  // 4
        0b1101101,  // 5
        0b1111101,  // 6
        0b0000111,  // 7
        0b1111111,  // 8
        0b1101111,  // 9
        0b1110111,  // A
        0b1111100,  // B
        0b0111001,  // C
        0b1011110,  // D
        0b1111001,  // E
        0b1110001,  // F
    // }}}
    };
    
    // advance to next digit
    if (display > 6) {
        display = 0;
    }

    // determine digit
    uint8_t v = 0;
    switch (display) {
        case 0: v = (address >> 12) & 0xF; break;
        case 1: v = (address >> 8) & 0xF; break;
        case 2: v = (address >> 4) & 0xF; break;
        case 3: v = address & 0xF; break;
        case 4: v = (data >> 4) & 0xF; break;
        case 5: v = data & 0xF; break;
    };
    
    uint8_t p = PORTD & (1 << PD7);

    // display digit
    //PORTC = 0;
    PORTD = p | digits[v];
    PORTC = (1 << display);

    ++display;
}

// }}}

int
main()
{
    prepare_pins();
    prepare_timer();

    // TODO - change this busy loop to a sleep mode?
    for (;;) {
        if (event.readbus) {
            read_bus();
            event.readbus = false;
        } else if (event.btdetect) {
            button_detect();
            event.btdetect = false;
        } else if (event.refresh) {
            display_refresh();
            event.refresh = false;
        }
    }
}

// vim: ts=4:sts=4:sw=4:expandtab:foldmethod=marker
