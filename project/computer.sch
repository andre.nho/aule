EESchema Schematic File Version 2
LIBS:computer-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:vga
LIBS:computer-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 5
Title ""
Date "11 dec 2016"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 7550 700  750  2250
U 5842D235
F0 "CPU" 60
F1 "cpu.sch" 60
$EndSheet
$Sheet
S 9350 700  750  2250
U 5842D24A
F0 "Video" 60
F1 "video.sch" 60
$EndSheet
$Sheet
S 10250 700  750  2250
U 5842D259
F0 "Devices" 60
F1 "devices.sch" 60
$EndSheet
$Sheet
S 8450 700  750  2250
U 5842D276
F0 "Debug" 60
F1 "debug.sch" 60
$EndSheet
$Comp
L CONN_02X12 P1
U 1 1 5842D35B
P 1650 3550
F 0 "P1" H 1650 4200 50  0000 C CNN
F 1 "CONN_02X12" V 1650 3550 50  0000 C CNN
F 2 "" H 1650 2350 50  0000 C CNN
F 3 "" H 1650 2350 50  0000 C CNN
	1    1650 3550
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR1
U 1 1 5842D593
P 1200 2950
F 0 "#PWR1" H 1200 2800 50  0001 C CNN
F 1 "VCC" H 1200 3100 50  0000 C CNN
F 2 "" H 1200 2950 50  0000 C CNN
F 3 "" H 1200 2950 50  0000 C CNN
	1    1200 2950
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-computer #PWR3
U 1 1 5842D5E1
P 2100 4150
F 0 "#PWR3" H 2100 3900 50  0001 C CNN
F 1 "GND" H 2100 4000 50  0000 C CNN
F 2 "" H 2100 4150 50  0000 C CNN
F 3 "" H 2100 4150 50  0000 C CNN
	1    2100 4150
	1    0    0    -1  
$EndComp
Text GLabel 1300 3200 0    60   Input ~ 0
D0
Text GLabel 1300 3300 0    60   Input ~ 0
D1
Text GLabel 1300 3400 0    60   Input ~ 0
D2
Text GLabel 1300 3500 0    60   Input ~ 0
D3
Text GLabel 1300 3600 0    60   Input ~ 0
D4
Text GLabel 1300 3700 0    60   Input ~ 0
D5
Text GLabel 1300 3800 0    60   Input ~ 0
D6
Text GLabel 1300 3900 0    60   Input ~ 0
D7
Text GLabel 1300 4000 0    60   Input ~ 0
~RESET
Text GLabel 2000 3000 2    60   Input ~ 0
A0
Text GLabel 2000 3100 2    60   Input ~ 0
A1
Text GLabel 2000 3200 2    60   Input ~ 0
A2
Text GLabel 2000 3300 2    60   Input ~ 0
A3
Text GLabel 2000 3400 2    60   Input ~ 0
~CLK/4
Text GLabel 2000 3500 2    60   Input ~ 0
~CLK/2
Text GLabel 2000 3600 2    60   Input ~ 0
~CLK
Text GLabel 2000 3700 2    60   Input ~ 0
~IORQ
Text GLabel 2000 3800 2    60   Input ~ 0
~RD
Text GLabel 2000 3900 2    60   Input ~ 0
~WR
Text Notes 2050 4550 2    60   ~ 0
CONNECTORS (x4)
$Comp
L BARREL_JACK CON1
U 1 1 5842D9C8
P 1250 1400
F 0 "CON1" H 1250 1650 50  0000 C CNN
F 1 "BARREL_JACK" H 1250 1200 50  0000 C CNN
F 2 ">=7.5V  1A" H 1250 1050 50  0000 C CNN
F 3 "" H 1250 1400 50  0000 C CNN
	1    1250 1400
	1    0    0    -1  
$EndComp
$Comp
L 7805 U3
U 1 1 5842DA51
P 4350 1350
F 0 "U3" H 4500 1154 50  0000 C CNN
F 1 "7805" H 4350 1550 50  0000 C CNN
F 2 "" H 4350 1350 50  0000 C CNN
F 3 "" H 4350 1350 50  0000 C CNN
	1    4350 1350
	1    0    0    -1  
$EndComp
$Comp
L C-RESCUE-computer C1
U 1 1 5842DB7F
P 3950 1500
F 0 "C1" H 3975 1600 50  0000 L CNN
F 1 "C" H 3975 1400 50  0000 L CNN
F 2 "0,33uF" H 3800 1400 50  0000 C CNN
F 3 "" H 3950 1500 50  0000 C CNN
	1    3950 1500
	1    0    0    -1  
$EndComp
$Comp
L C-RESCUE-computer C2
U 1 1 5842DBD8
P 4850 1500
F 0 "C2" H 4875 1600 50  0000 L CNN
F 1 "C" H 4875 1400 50  0000 L CNN
F 2 "0,1uF" H 4700 1400 50  0000 C CNN
F 3 "" H 4850 1500 50  0000 C CNN
	1    4850 1500
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR10
U 1 1 5842DF4E
P 5450 1100
F 0 "#PWR10" H 5450 950 50  0001 C CNN
F 1 "VCC" H 5450 1250 50  0000 C CNN
F 2 "" H 5450 1100 50  0000 C CNN
F 3 "" H 5450 1100 50  0000 C CNN
	1    5450 1100
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-computer #PWR8
U 1 1 5842DF9E
P 4350 1800
F 0 "#PWR8" H 4350 1550 50  0001 C CNN
F 1 "GND" H 4350 1650 50  0000 C CNN
F 2 "" H 4350 1800 50  0000 C CNN
F 3 "" H 4350 1800 50  0000 C CNN
	1    4350 1800
	1    0    0    -1  
$EndComp
Text Notes 3350 2000 2    60   ~ 0
POWER SUPPLY
$Comp
L D D1
U 1 1 5842E202
P 3650 1300
F 0 "D1" H 3650 1400 50  0000 C CNN
F 1 "D" H 3650 1200 50  0000 C CNN
F 2 "1N4001" H 3650 1100 50  0000 C CNN
F 3 "" H 3650 1300 50  0000 C CNN
	1    3650 1300
	-1   0    0    1   
$EndComp
Text Notes 3900 2150 2    60   ~ 0
(cannot supply more than 1A)
$Comp
L FUSE F1
U 1 1 5842E390
P 3100 1300
F 0 "F1" H 3200 1350 50  0000 C CNN
F 1 "FUSE" H 3000 1250 50  0000 C CNN
F 2 "1A" H 3100 1450 50  0000 C CNN
F 3 "" H 3100 1300 50  0000 C CNN
	1    3100 1300
	1    0    0    -1  
$EndComp
$Comp
L LED-RESCUE-computer D2
U 1 1 5842E903
P 5800 1300
F 0 "D2" H 5800 1400 50  0000 C CNN
F 1 "LED" H 5800 1200 50  0000 C CNN
F 2 "GREEN" H 5800 1100 50  0000 C CNN
F 3 "" H 5800 1300 50  0000 C CNN
	1    5800 1300
	-1   0    0    1   
$EndComp
$Comp
L R-RESCUE-computer R3
U 1 1 5842E969
P 6000 1650
F 0 "R3" V 6080 1650 50  0000 C CNN
F 1 "R" V 6000 1650 50  0000 C CNN
F 2 "150" V 5930 1650 50  0000 C CNN
F 3 "" H 6000 1650 50  0000 C CNN
	1    6000 1650
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-computer #PWR11
U 1 1 5842E9F4
P 6000 1900
F 0 "#PWR11" H 6000 1650 50  0001 C CNN
F 1 "GND" H 6000 1750 50  0000 C CNN
F 2 "" H 6000 1900 50  0000 C CNN
F 3 "" H 6000 1900 50  0000 C CNN
	1    6000 1900
	1    0    0    -1  
$EndComp
NoConn ~ 1400 5800
$Comp
L GND-RESCUE-computer #PWR2
U 1 1 5844063F
P 1350 6000
F 0 "#PWR2" H 1350 5750 50  0001 C CNN
F 1 "GND" H 1350 5850 50  0000 C CNN
F 2 "" H 1350 6000 50  0000 C CNN
F 3 "" H 1350 6000 50  0000 C CNN
	1    1350 6000
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR4
U 1 1 58440645
P 2200 5700
F 0 "#PWR4" H 2200 5550 50  0001 C CNN
F 1 "VCC" H 2200 5850 50  0000 C CNN
F 2 "" H 2200 5700 50  0000 C CNN
F 3 "" H 2200 5700 50  0000 C CNN
	1    2200 5700
	1    0    0    -1  
$EndComp
$Comp
L CO1100 U1
U 1 1 5844064B
P 1750 5850
F 0 "U1" H 1550 6050 50  0000 L CNN
F 1 "CO1100" H 1750 6050 50  0000 L CNN
F 2 "25.175 MHz" H 1550 5600 50  0000 L CIN
F 3 "" H 1725 5850 50  0000 L CNN
	1    1750 5850
	1    0    0    -1  
$EndComp
$Comp
L 74HC74 U4
U 1 1 58440652
P 4550 5950
F 0 "U4" H 4700 6250 50  0000 C CNN
F 1 "74HC74" H 4850 5655 50  0000 C CNN
F 2 "" H 4550 5950 50  0000 C CNN
F 3 "" H 4550 5950 50  0000 C CNN
	1    4550 5950
	1    0    0    -1  
$EndComp
$Comp
L 74HC74 U4
U 2 1 58440659
P 7950 5750
F 0 "U4" H 8100 6050 50  0000 C CNN
F 1 "74HC74" H 8250 5455 50  0000 C CNN
F 2 "" H 7950 5750 50  0000 C CNN
F 3 "" H 7950 5750 50  0000 C CNN
	2    7950 5750
	1    0    0    -1  
$EndComp
Text GLabel 3450 5650 2    60   Input ~ 0
~CLK
Text GLabel 6750 5500 2    60   Input ~ 0
~CLK/2
Text GLabel 10100 5550 2    60   Input ~ 0
~CLK/4
Text Notes 4250 6850 2    60   ~ 0
CLOCK GENERATOR
Text Notes 5200 7050 2    60   ~ 0
(TODO: does it needs the NOT ports and the pull ups?)
$Comp
L VCC #PWR7
U 1 1 58440665
P 3750 4900
F 0 "#PWR7" H 3750 4750 50  0001 C CNN
F 1 "VCC" H 3750 5050 50  0000 C CNN
F 2 "" H 3750 4900 50  0000 C CNN
F 3 "" H 3750 4900 50  0000 C CNN
	1    3750 4900
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR13
U 1 1 5844066B
P 7150 4750
F 0 "#PWR13" H 7150 4600 50  0001 C CNN
F 1 "VCC" H 7150 4900 50  0000 C CNN
F 2 "" H 7150 4750 50  0000 C CNN
F 3 "" H 7150 4750 50  0000 C CNN
	1    7150 4750
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-computer #PWR6
U 1 1 58440E8B
P 3700 3750
F 0 "#PWR6" H 3700 3500 50  0001 C CNN
F 1 "GND" H 3700 3600 50  0000 C CNN
F 2 "" H 3700 3750 50  0000 C CNN
F 3 "" H 3700 3750 50  0000 C CNN
	1    3700 3750
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH SW2
U 1 1 5844108D
P 4150 3550
F 0 "SW2" H 4300 3660 50  0000 C CNN
F 1 "SW_PUSH" H 4150 3470 50  0000 C CNN
F 2 "RESET" H 4150 3350 50  0000 C CNN
F 3 "" H 4150 3550 50  0000 C CNN
	1    4150 3550
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-computer R2
U 1 1 5844143E
P 4650 3150
F 0 "R2" V 4730 3150 50  0000 C CNN
F 1 "R" V 4650 3150 50  0000 C CNN
F 2 "2k2" V 4550 3150 50  0000 C CNN
F 3 "" H 4650 3150 50  0000 C CNN
	1    4650 3150
	1    0    0    -1  
$EndComp
$Comp
L 74HC14 U2
U 2 1 5844165E
P 5200 3550
F 0 "U2" H 5350 3650 50  0000 C CNN
F 1 "74HC14" H 5400 3450 50  0000 C CNN
F 2 "" H 5200 3550 50  0000 C CNN
F 3 "" H 5200 3550 50  0000 C CNN
	2    5200 3550
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR9
U 1 1 58441911
P 4650 2850
F 0 "#PWR9" H 4650 2700 50  0001 C CNN
F 1 "VCC" H 4650 3000 50  0000 C CNN
F 2 "" H 4650 2850 50  0000 C CNN
F 3 "" H 4650 2850 50  0000 C CNN
	1    4650 2850
	1    0    0    -1  
$EndComp
$Comp
L 74HC14 U2
U 4 1 58441F3D
P 6200 3550
F 0 "U2" H 6350 3650 50  0000 C CNN
F 1 "74HC14" H 6400 3450 50  0000 C CNN
F 2 "" H 6200 3550 50  0000 C CNN
F 3 "" H 6200 3550 50  0000 C CNN
	4    6200 3550
	1    0    0    -1  
$EndComp
Text GLabel 6900 3550 2    60   Input ~ 0
~RESET
Text Notes 5700 4100 2    60   ~ 0
RESET CIRCUIT
$Comp
L 74HC14 U2
U 1 1 58445F39
P 2900 5650
F 0 "U2" H 3050 5750 50  0000 C CNN
F 1 "74HC14" H 3100 5550 50  0000 C CNN
F 2 "" H 2900 5650 50  0000 C CNN
F 3 "" H 2900 5650 50  0000 C CNN
	1    2900 5650
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-computer R1
U 1 1 58446423
P 3400 5250
F 0 "R1" V 3480 5250 50  0000 C CNN
F 1 "R" V 3400 5250 50  0000 C CNN
F 2 "330" V 3300 5250 50  0000 C CNN
F 3 "" H 3400 5250 50  0000 C CNN
	1    3400 5250
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR5
U 1 1 58446682
P 3400 4900
F 0 "#PWR5" H 3400 4750 50  0001 C CNN
F 1 "VCC" H 3400 5050 50  0000 C CNN
F 2 "" H 3400 4900 50  0000 C CNN
F 3 "" H 3400 4900 50  0000 C CNN
	1    3400 4900
	1    0    0    -1  
$EndComp
$Comp
L 74HC14 U2
U 3 1 58446F8D
P 6000 5500
F 0 "U2" H 6150 5600 50  0000 C CNN
F 1 "74HC14" H 6200 5400 50  0000 C CNN
F 2 "" H 6000 5500 50  0000 C CNN
F 3 "" H 6000 5500 50  0000 C CNN
	3    6000 5500
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-computer R4
U 1 1 58448504
P 6600 5100
F 0 "R4" V 6680 5100 50  0000 C CNN
F 1 "R" V 6600 5100 50  0000 C CNN
F 2 "330" V 6500 5100 50  0000 C CNN
F 3 "" H 6600 5100 50  0000 C CNN
	1    6600 5100
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR12
U 1 1 5844850A
P 6600 4750
F 0 "#PWR12" H 6600 4600 50  0001 C CNN
F 1 "VCC" H 6600 4900 50  0000 C CNN
F 2 "" H 6600 4750 50  0000 C CNN
F 3 "" H 6600 4750 50  0000 C CNN
	1    6600 4750
	1    0    0    -1  
$EndComp
$Comp
L 74HC14 U2
U 5 1 58448BCA
P 9350 5550
F 0 "U2" H 9500 5650 50  0000 C CNN
F 1 "74HC14" H 9550 5450 50  0000 C CNN
F 2 "" H 9350 5550 50  0000 C CNN
F 3 "" H 9350 5550 50  0000 C CNN
	5    9350 5550
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-computer R5
U 1 1 58448BD1
P 9950 5150
F 0 "R5" V 10030 5150 50  0000 C CNN
F 1 "R" V 9950 5150 50  0000 C CNN
F 2 "330" V 9850 5150 50  0000 C CNN
F 3 "" H 9950 5150 50  0000 C CNN
	1    9950 5150
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR14
U 1 1 58448BD7
P 9950 4800
F 0 "#PWR14" H 9950 4650 50  0001 C CNN
F 1 "VCC" H 9950 4950 50  0000 C CNN
F 2 "" H 9950 4800 50  0000 C CNN
F 3 "" H 9950 4800 50  0000 C CNN
	1    9950 4800
	1    0    0    -1  
$EndComp
$Comp
L SPST SW1
U 1 1 5844E551
P 2250 1300
F 0 "SW1" H 2250 1400 50  0000 C CNN
F 1 "SPST" H 2250 1200 50  0000 C CNN
F 2 "" H 2250 1300 50  0000 C CNN
F 3 "" H 2250 1300 50  0000 C CNN
	1    2250 1300
	1    0    0    -1  
$EndComp
$Comp
L CP1-RESCUE-computer C3
U 1 1 58450AEF
P 5250 1500
F 0 "C3" H 5275 1600 50  0000 L CNN
F 1 "CP1" H 5275 1400 50  0000 L CNN
F 2 "10uF" H 5150 1400 50  0000 C CNN
F 3 "" H 5250 1500 50  0000 C CNN
	1    5250 1500
	1    0    0    -1  
$EndComp
Text Notes 750  7500 0    60   ~ 0
External ports:\n * Power outlet\n * Power button\n * Power LED\n * Reset button
Text GLabel 1300 4100 0    60   Input ~ 0
RESET
Text GLabel 6900 3000 2    60   Input ~ 0
RESET
Wire Wire Line
	1200 3000 1400 3000
Wire Wire Line
	1300 3200 1400 3200
Wire Wire Line
	1400 3300 1300 3300
Wire Wire Line
	1400 3400 1300 3400
Wire Wire Line
	1400 3500 1300 3500
Wire Wire Line
	1400 3600 1300 3600
Wire Wire Line
	1400 3700 1300 3700
Wire Wire Line
	1400 3800 1300 3800
Wire Wire Line
	1400 3900 1300 3900
Wire Wire Line
	1400 4000 1300 4000
Wire Wire Line
	1900 3500 2000 3500
Wire Wire Line
	1900 3300 2000 3300
Wire Wire Line
	1900 3200 2000 3200
Wire Wire Line
	1900 3100 2000 3100
Wire Wire Line
	1900 3600 2000 3600
Wire Wire Line
	1900 3700 2000 3700
Wire Wire Line
	1900 3800 2000 3800
Wire Wire Line
	1900 3900 2000 3900
Wire Wire Line
	1900 4000 2000 4000
Wire Wire Line
	1900 4100 2100 4100
Wire Wire Line
	1200 3000 1200 2950
Wire Wire Line
	2100 4100 2100 4150
Wire Wire Line
	1900 3000 2000 3000
Wire Wire Line
	1900 3400 2000 3400
Wire Wire Line
	4750 1300 5600 1300
Connection ~ 4850 1700
Wire Wire Line
	1550 1400 1550 1700
Connection ~ 3950 1700
Connection ~ 1550 1500
Wire Wire Line
	5450 1300 5450 1100
Connection ~ 5250 1300
Wire Wire Line
	4350 1600 4350 1800
Wire Wire Line
	1550 1700 5250 1700
Wire Wire Line
	3800 1300 3950 1300
Wire Wire Line
	3350 1300 3500 1300
Connection ~ 5450 1300
Wire Wire Line
	6000 1500 6000 1300
Wire Wire Line
	6000 1900 6000 1800
Wire Wire Line
	1350 6000 1350 5950
Wire Wire Line
	1350 5950 1400 5950
Wire Wire Line
	2100 5800 2200 5800
Wire Wire Line
	2200 5800 2200 5700
Wire Wire Line
	2100 5950 3950 5950
Wire Wire Line
	5150 6150 5250 6150
Wire Wire Line
	5250 6150 5250 5250
Wire Wire Line
	5250 5250 3950 5250
Wire Wire Line
	3950 5250 3950 5750
Wire Wire Line
	5150 5750 7350 5750
Wire Wire Line
	8550 5950 8700 5950
Wire Wire Line
	8700 5950 8700 5050
Wire Wire Line
	8700 5050 7350 5050
Wire Wire Line
	7350 5050 7350 5550
Wire Wire Line
	8550 5550 8900 5550
Wire Wire Line
	2350 5650 2350 5950
Connection ~ 2350 5950
Wire Wire Line
	5450 5750 5450 5500
Connection ~ 5450 5750
Wire Wire Line
	3750 4900 3750 6500
Wire Wire Line
	3750 6500 4550 6500
Wire Wire Line
	4550 5400 4550 5050
Wire Wire Line
	4550 5050 3750 5050
Connection ~ 3750 5050
Wire Wire Line
	7150 4750 7150 6300
Wire Wire Line
	7150 6300 7950 6300
Wire Wire Line
	7950 5200 7950 4850
Wire Wire Line
	7950 4850 7150 4850
Connection ~ 7150 4850
Wire Wire Line
	3700 3750 3700 3550
Wire Wire Line
	3700 3550 3850 3550
Wire Wire Line
	5650 3550 5750 3550
Wire Wire Line
	6900 3550 6650 3550
Wire Wire Line
	5700 3550 5700 3000
Wire Wire Line
	5700 3000 6900 3000
Connection ~ 5700 3550
Wire Wire Line
	4650 3000 4650 2850
Wire Wire Line
	4450 3550 4750 3550
Wire Wire Line
	4650 3550 4650 3300
Connection ~ 4650 3550
Wire Wire Line
	2350 5650 2450 5650
Wire Wire Line
	3350 5650 3450 5650
Wire Wire Line
	3400 5650 3400 5400
Connection ~ 3400 5650
Wire Wire Line
	3400 4900 3400 5100
Wire Wire Line
	5450 5500 5550 5500
Wire Wire Line
	6450 5500 6750 5500
Wire Wire Line
	6600 4750 6600 4950
Wire Wire Line
	6600 5250 6600 5500
Connection ~ 6600 5500
Wire Wire Line
	9800 5550 10100 5550
Wire Wire Line
	9950 4800 9950 5000
Wire Wire Line
	9950 5300 9950 5550
Connection ~ 9950 5550
Wire Wire Line
	1550 1300 1750 1300
Wire Wire Line
	2750 1300 2850 1300
Connection ~ 4350 1700
Wire Wire Line
	4850 1350 4850 1300
Connection ~ 4850 1300
Wire Wire Line
	4850 1650 4850 1700
Wire Wire Line
	3950 1650 3950 1700
Wire Wire Line
	3950 1300 3950 1350
Wire Wire Line
	5250 1700 5250 1650
Wire Wire Line
	5250 1300 5250 1350
Wire Wire Line
	1400 4100 1300 4100
NoConn ~ 1400 3100
$EndSCHEMATC
