EESchema Schematic File Version 2
LIBS:computer-rescue
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:vga
LIBS:computer-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date "11 dec 2016"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CA56-12 AFF1
U 1 1 5847F648
P 5050 2100
F 0 "AFF1" H 5050 2900 50  0000 C CNN
F 1 "CA56-12" H 5050 2800 50  0000 C CNN
F 2 "" H 4550 2100 50  0000 C CNN
F 3 "" H 4550 2100 50  0000 C CNN
	1    5050 2100
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-computer #PWR69
U 1 1 58480930
P 10850 3200
F 0 "#PWR69" H 10850 2950 50  0001 C CNN
F 1 "GND" H 10850 3050 50  0000 C CNN
F 2 "" H 10850 3200 50  0000 C CNN
F 3 "" H 10850 3200 50  0000 C CNN
	1    10850 3200
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR68
U 1 1 58480936
P 9950 1200
F 0 "#PWR68" H 9950 1050 50  0001 C CNN
F 1 "VCC" H 9950 1350 50  0000 C CNN
F 2 "" H 9950 1200 50  0000 C CNN
F 3 "" H 9950 1200 50  0000 C CNN
	1    9950 1200
	1    0    0    -1  
$EndComp
Text GLabel 10050 1450 0    60   Input ~ 0
D0
Text GLabel 10050 1550 0    60   Input ~ 0
D1
Text GLabel 10050 1650 0    60   Input ~ 0
D2
Text GLabel 10050 1750 0    60   Input ~ 0
D3
Text GLabel 10050 1850 0    60   Input ~ 0
D4
Text GLabel 10050 1950 0    60   Input ~ 0
D5
Text GLabel 10050 2050 0    60   Input ~ 0
D6
Text GLabel 10050 2150 0    60   Input ~ 0
D7
Text GLabel 10750 1250 2    60   Input ~ 0
A0
Text GLabel 10750 1350 2    60   Input ~ 0
A1
Text GLabel 10750 1450 2    60   Input ~ 0
A2
Text GLabel 10750 1550 2    60   Input ~ 0
A3
Text GLabel 10750 1650 2    60   Input ~ 0
A4
Text GLabel 10750 1750 2    60   Input ~ 0
A5
Text GLabel 10750 1850 2    60   Input ~ 0
A6
Text GLabel 10750 1950 2    60   Input ~ 0
A7
Text GLabel 10750 2050 2    60   Input ~ 0
A8
Text GLabel 10750 2150 2    60   Input ~ 0
A9
Text GLabel 10750 2250 2    60   Input ~ 0
A10
Text GLabel 10750 2350 2    60   Input ~ 0
A11
Text GLabel 10750 2450 2    60   Input ~ 0
A12
Text GLabel 10750 2550 2    60   Input ~ 0
A13
Text GLabel 10750 2650 2    60   Input ~ 0
A14
Text GLabel 10750 2750 2    60   Input ~ 0
A15
Text GLabel 10050 2450 0    60   Input ~ 0
~MREQ
Text GLabel 10050 2550 0    60   Input ~ 0
~IORQ
Text GLabel 10050 2650 0    60   Input ~ 0
~RD
Text GLabel 10050 2750 0    60   Input ~ 0
~WR
Text GLabel 10050 2850 0    60   Input ~ 0
~M1
Text GLabel 10050 2950 0    60   Input ~ 0
~EXTCLK
Text GLabel 10050 3050 0    60   Input ~ 0
~DBGCLK
Text GLabel 10050 2350 0    60   Input ~ 0
~RESET
Text Notes 9950 3550 0    60   ~ 0
Connector to debugger
Text GLabel 10750 2850 2    60   Input ~ 0
~HALT
$Comp
L CONN_02X20 P11
U 1 1 5848095F
P 10400 2200
F 0 "P11" H 10400 3250 50  0000 C CNN
F 1 "CONN_02X20" V 10400 2200 50  0000 C CNN
F 2 "" H 10400 1250 50  0000 C CNN
F 3 "" H 10400 1250 50  0000 C CNN
	1    10400 2200
	1    0    0    -1  
$EndComp
NoConn ~ 10050 3150
NoConn ~ 10800 2950
$Comp
L ULN2003 U23
U 1 1 58481427
P 9050 5000
F 0 "U23" H 9050 5100 50  0000 C CNN
F 1 "ULN2003" H 9050 4900 50  0000 C CNN
F 2 "" H 9050 5000 50  0000 C CNN
F 3 "" H 9050 5000 50  0000 C CNN
	1    9050 5000
	1    0    0    -1  
$EndComp
Text GLabel 7100 4600 0    60   Input ~ 0
~MREQ
Text GLabel 7100 4950 0    60   Input ~ 0
~IORQ
Text GLabel 7100 5300 0    60   Input ~ 0
~RD
Text GLabel 7100 5650 0    60   Input ~ 0
~WR
Text GLabel 7100 6000 0    60   Input ~ 0
~M1
Text GLabel 7100 6350 0    60   Input ~ 0
~HALT
$Comp
L LED-RESCUE-computer D6
U 1 1 584845D7
P 10600 4600
F 0 "D6" H 10600 4700 50  0000 C CNN
F 1 "LED" H 10600 4500 50  0000 C CNN
F 2 "" H 10600 4600 50  0000 C CNN
F 3 "" H 10600 4600 50  0000 C CNN
	1    10600 4600
	1    0    0    -1  
$EndComp
$Comp
L LED-RESCUE-computer D7
U 1 1 58484675
P 10600 4850
F 0 "D7" H 10600 4950 50  0000 C CNN
F 1 "LED" H 10600 4750 50  0000 C CNN
F 2 "" H 10600 4850 50  0000 C CNN
F 3 "" H 10600 4850 50  0000 C CNN
	1    10600 4850
	1    0    0    -1  
$EndComp
$Comp
L LED-RESCUE-computer D8
U 1 1 584846BD
P 10600 5100
F 0 "D8" H 10600 5200 50  0000 C CNN
F 1 "LED" H 10600 5000 50  0000 C CNN
F 2 "" H 10600 5100 50  0000 C CNN
F 3 "" H 10600 5100 50  0000 C CNN
	1    10600 5100
	1    0    0    -1  
$EndComp
$Comp
L LED-RESCUE-computer D9
U 1 1 58484708
P 10600 5350
F 0 "D9" H 10600 5450 50  0000 C CNN
F 1 "LED" H 10600 5250 50  0000 C CNN
F 2 "" H 10600 5350 50  0000 C CNN
F 3 "" H 10600 5350 50  0000 C CNN
	1    10600 5350
	1    0    0    -1  
$EndComp
$Comp
L LED-RESCUE-computer D10
U 1 1 5848475A
P 10600 5600
F 0 "D10" H 10600 5700 50  0000 C CNN
F 1 "LED" H 10600 5500 50  0000 C CNN
F 2 "" H 10600 5600 50  0000 C CNN
F 3 "" H 10600 5600 50  0000 C CNN
	1    10600 5600
	1    0    0    -1  
$EndComp
$Comp
L LED-RESCUE-computer D11
U 1 1 584847F2
P 10600 5850
F 0 "D11" H 10600 5950 50  0000 C CNN
F 1 "LED" H 10600 5750 50  0000 C CNN
F 2 "" H 10600 5850 50  0000 C CNN
F 3 "" H 10600 5850 50  0000 C CNN
	1    10600 5850
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR70
U 1 1 58485888
P 10950 4400
F 0 "#PWR70" H 10950 4250 50  0001 C CNN
F 1 "VCC" H 10950 4550 50  0000 C CNN
F 2 "" H 10950 4400 50  0000 C CNN
F 3 "" H 10950 4400 50  0000 C CNN
	1    10950 4400
	1    0    0    -1  
$EndComp
NoConn ~ 9700 5200
NoConn ~ 8400 5200
NoConn ~ 9700 5400
$Comp
L ATMEGA8-P IC5
U 1 1 58488AE0
P 4250 5950
F 0 "IC5" H 3500 7250 50  0000 L BNN
F 1 "ATMEGA8-P" H 4750 4500 50  0000 L BNN
F 2 "DIL28" H 4250 5950 50  0000 C CIN
F 3 "" H 4250 5950 50  0000 C CNN
	1    4250 5950
	1    0    0    -1  
$EndComp
$Comp
L 74LS165 U18
U 1 1 58489C8D
P 2150 1650
F 0 "U18" H 2300 1600 50  0000 C CNN
F 1 "74HC165" H 2300 1400 50  0000 C CNN
F 2 "" H 2150 1650 50  0000 C CNN
F 3 "" H 2150 1650 50  0000 C CNN
	1    2150 1650
	1    0    0    -1  
$EndComp
$Comp
L 74LS165 U19
U 1 1 58489EDC
P 2150 3150
F 0 "U19" H 2300 3100 50  0000 C CNN
F 1 "74HC165" H 2300 2900 50  0000 C CNN
F 2 "" H 2150 3150 50  0000 C CNN
F 3 "" H 2150 3150 50  0000 C CNN
	1    2150 3150
	1    0    0    -1  
$EndComp
$Comp
L 74LS165 U20
U 1 1 5848A40E
P 2150 4650
F 0 "U20" H 2300 4600 50  0000 C CNN
F 1 "74HC165" H 2300 4400 50  0000 C CNN
F 2 "" H 2150 4650 50  0000 C CNN
F 3 "" H 2150 4650 50  0000 C CNN
	1    2150 4650
	1    0    0    -1  
$EndComp
NoConn ~ 1450 1050
Text GLabel 900  1150 0    60   Input ~ 0
A0
Text GLabel 900  1250 0    60   Input ~ 0
A1
Text GLabel 900  1350 0    60   Input ~ 0
A2
Text GLabel 900  1450 0    60   Input ~ 0
A3
Text GLabel 900  1550 0    60   Input ~ 0
A4
Text GLabel 900  1650 0    60   Input ~ 0
A5
Text GLabel 900  1750 0    60   Input ~ 0
A6
Text GLabel 900  1850 0    60   Input ~ 0
A7
Text GLabel 900  2650 0    60   Input ~ 0
A8
Text GLabel 900  2750 0    60   Input ~ 0
A9
Text GLabel 900  2850 0    60   Input ~ 0
A10
Text GLabel 900  2950 0    60   Input ~ 0
A11
Text GLabel 900  3050 0    60   Input ~ 0
A12
Text GLabel 900  3150 0    60   Input ~ 0
A13
Text GLabel 900  3250 0    60   Input ~ 0
A14
Text GLabel 900  3350 0    60   Input ~ 0
A15
Text GLabel 900  4150 0    60   Input ~ 0
D0
Text GLabel 900  4250 0    60   Input ~ 0
D1
Text GLabel 900  4350 0    60   Input ~ 0
D2
Text GLabel 900  4450 0    60   Input ~ 0
D3
Text GLabel 900  4550 0    60   Input ~ 0
D4
Text GLabel 900  4650 0    60   Input ~ 0
D5
Text GLabel 900  4750 0    60   Input ~ 0
D6
Text GLabel 900  4850 0    60   Input ~ 0
D7
NoConn ~ 2850 1250
NoConn ~ 2850 2750
NoConn ~ 2850 4250
$Comp
L GND-RESCUE-computer #PWR59
U 1 1 5848D6D8
P 1100 5400
F 0 "#PWR59" H 1100 5150 50  0001 C CNN
F 1 "GND" H 1100 5250 50  0000 C CNN
F 2 "" H 1100 5400 50  0000 C CNN
F 3 "" H 1100 5400 50  0000 C CNN
	1    1100 5400
	1    0    0    -1  
$EndComp
Entry Wire Line
	5550 5550 5650 5450
Entry Wire Line
	5550 5650 5650 5550
Entry Wire Line
	5550 5750 5650 5650
Entry Wire Line
	5550 5850 5650 5750
Entry Wire Line
	5550 5950 5650 5850
Entry Wire Line
	5550 6050 5650 5950
Entry Wire Line
	5800 6250 5900 6150
Entry Wire Line
	5800 6350 5900 6250
Entry Wire Line
	5800 6450 5900 6350
Entry Wire Line
	5800 6550 5900 6450
Entry Wire Line
	5800 6650 5900 6550
Entry Wire Line
	5800 6750 5900 6650
Entry Wire Line
	5800 6850 5900 6750
Entry Wire Line
	3950 800  4050 900 
Entry Wire Line
	4450 800  4550 900 
$Comp
L SW_PUSH_SMALL_H SW5
U 1 1 5849255A
P 6300 4950
F 0 "SW5" H 6380 5060 50  0000 C CNN
F 1 "~" H 6660 4890 50  0000 C CNN
F 2 "STEP" H 6300 5150 50  0000 C CNN
F 3 "" H 6300 5150 50  0000 C CNN
	1    6300 4950
	1    0    0    -1  
$EndComp
$Comp
L SW_PUSH_SMALL_H SW6
U 1 1 58492677
P 6300 5250
F 0 "SW6" H 6380 5360 50  0000 C CNN
F 1 "~" H 6660 5190 50  0000 C CNN
F 2 "CYCLE" H 6300 5450 50  0000 C CNN
F 3 "" H 6300 5450 50  0000 C CNN
	1    6300 5250
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-computer #PWR65
U 1 1 584926E1
P 6500 5350
F 0 "#PWR65" H 6500 5100 50  0001 C CNN
F 1 "GND" H 6500 5200 50  0000 C CNN
F 2 "" H 6500 5350 50  0000 C CNN
F 3 "" H 6500 5350 50  0000 C CNN
	1    6500 5350
	1    0    0    -1  
$EndComp
Text GLabel 5950 5350 2    60   Input ~ 0
M1
Text GLabel 5400 6950 2    60   Input ~ 0
~EXTCLK
$Comp
L GND-RESCUE-computer #PWR63
U 1 1 58498037
P 4250 7550
F 0 "#PWR63" H 4250 7300 50  0001 C CNN
F 1 "GND" H 4250 7400 50  0000 C CNN
F 2 "" H 4250 7550 50  0000 C CNN
F 3 "" H 4250 7550 50  0000 C CNN
	1    4250 7550
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR62
U 1 1 58498998
P 4250 4550
F 0 "#PWR62" H 4250 4400 50  0001 C CNN
F 1 "VCC" H 4250 4700 50  0000 C CNN
F 2 "" H 4250 4550 50  0000 C CNN
F 3 "" H 4250 4550 50  0000 C CNN
	1    4250 4550
	1    0    0    -1  
$EndComp
$Comp
L Switch_SPDT_x2 SW4
U 1 1 5849A4DE
P 1550 6550
F 0 "SW4" H 1350 6700 50  0000 C CNN
F 1 "Switch_SPDT_x2" H 1300 6400 50  0000 C CNN
F 2 "" H 1550 6550 50  0000 C CNN
F 3 "" H 1550 6550 50  0000 C CNN
	1    1550 6550
	-1   0    0    1   
$EndComp
$Comp
L VCC #PWR60
U 1 1 5849C04A
P 1150 6350
F 0 "#PWR60" H 1150 6200 50  0001 C CNN
F 1 "VCC" H 1150 6500 50  0000 C CNN
F 2 "" H 1150 6350 50  0000 C CNN
F 3 "" H 1150 6350 50  0000 C CNN
	1    1150 6350
	1    0    0    -1  
$EndComp
$Comp
L GND-RESCUE-computer #PWR61
U 1 1 5849C19B
P 1150 6750
F 0 "#PWR61" H 1150 6500 50  0001 C CNN
F 1 "GND" H 1150 6600 50  0000 C CNN
F 2 "" H 1150 6750 50  0000 C CNN
F 3 "" H 1150 6750 50  0000 C CNN
	1    1150 6750
	1    0    0    -1  
$EndComp
Text GLabel 2200 6550 2    60   Input ~ 0
~DBGCLK
$Comp
L CONN_02X03 P10
U 1 1 584A5BDC
P 6850 3650
F 0 "P10" H 6850 3850 50  0000 C CNN
F 1 "CONN_02X03" H 6850 3450 50  0000 C CNN
F 2 "" H 6850 2450 50  0000 C CNN
F 3 "" H 6850 2450 50  0000 C CNN
	1    6850 3650
	1    0    0    -1  
$EndComp
Text Label 7150 3550 0    60   ~ 0
VCC
Text Label 7150 3650 0    60   ~ 0
MOSI
Text Label 7150 3750 0    60   ~ 0
GND
Text Label 6350 3550 0    60   ~ 0
MISO
Text Label 6350 3650 0    60   ~ 0
SCK
Text Label 6350 3750 0    60   ~ 0
RST
$Comp
L GND-RESCUE-computer #PWR67
U 1 1 584A5BE9
P 7400 3950
F 0 "#PWR67" H 7400 3700 50  0001 C CNN
F 1 "GND" H 7400 3800 50  0000 C CNN
F 2 "" H 7400 3950 50  0000 C CNN
F 3 "" H 7400 3950 50  0000 C CNN
	1    7400 3950
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR66
U 1 1 584A5BEF
P 7400 3500
F 0 "#PWR66" H 7400 3350 50  0001 C CNN
F 1 "VCC" H 7400 3650 50  0000 C CNN
F 2 "" H 7400 3500 50  0000 C CNN
F 3 "" H 7400 3500 50  0000 C CNN
	1    7400 3500
	1    0    0    -1  
$EndComp
NoConn ~ 3350 5050
NoConn ~ 3350 5150
NoConn ~ 3350 5250
NoConn ~ 3350 5550
NoConn ~ 3350 5750
$Comp
L ULN2003 U22
U 1 1 5848AA1D
P 8700 3900
F 0 "U22" H 8700 4000 50  0000 C CNN
F 1 "ULN2003" H 8700 3800 50  0000 C CNN
F 2 "" H 8700 3900 50  0000 C CNN
F 3 "" H 8700 3900 50  0000 C CNN
	1    8700 3900
	1    0    0    -1  
$EndComp
Entry Wire Line
	9350 3500 9450 3400
Entry Wire Line
	9350 3600 9450 3500
Entry Wire Line
	9350 3700 9450 3600
Entry Wire Line
	9350 3800 9450 3700
Entry Wire Line
	9350 3900 9450 3800
Entry Wire Line
	9350 4000 9450 3900
NoConn ~ 9350 4300
Entry Wire Line
	7950 3400 8050 3500
Entry Wire Line
	7950 3500 8050 3600
Entry Wire Line
	7950 3600 8050 3700
Entry Wire Line
	7950 3700 8050 3800
Entry Wire Line
	7950 3800 8050 3900
Entry Wire Line
	7950 3900 8050 4000
$Comp
L DA04-11 AFF2
U 1 1 584D8EDA
P 7350 2000
F 0 "AFF2" H 7350 2500 60  0000 C CNN
F 1 "DA04-11" H 7350 1550 60  0000 C CNN
F 2 "~" H 7350 2000 60  0000 C CNN
F 3 "~" H 7350 2000 60  0000 C CNN
	1    7350 2000
	1    0    0    -1  
$EndComp
NoConn ~ 9350 4100
NoConn ~ 8050 4100
$Comp
L R-RESCUE-computer R19
U 1 1 584D90F8
P 4750 3050
F 0 "R19" V 4700 3250 40  0000 C CNN
F 1 "R" V 4757 3051 40  0000 C CNN
F 2 "100" V 4700 2850 30  0000 C CNN
F 3 "~" H 4750 3050 30  0000 C CNN
	1    4750 3050
	1    0    0    -1  
$EndComp
NoConn ~ 5450 2800
Entry Wire Line
	4750 3300 4850 3400
Entry Wire Line
	4850 3300 4950 3400
Entry Wire Line
	4950 3300 5050 3400
Entry Wire Line
	5050 3300 5150 3400
Entry Wire Line
	5150 3300 5250 3400
Entry Wire Line
	5250 3300 5350 3400
Entry Wire Line
	5350 3300 5450 3400
$Comp
L R-RESCUE-computer R36
U 1 1 584D9DBE
P 9100 2300
F 0 "R36" V 9150 2550 40  0000 C CNN
F 1 "R" V 9107 2301 40  0000 C CNN
F 2 "68" V 9150 2100 30  0000 C CNN
F 3 "~" H 9100 2300 30  0000 C CNN
	1    9100 2300
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-computer R20
U 1 1 584D9DE9
P 4850 3050
F 0 "R20" V 4800 3250 40  0000 C CNN
F 1 "R" V 4857 3051 40  0000 C CNN
F 2 "100" V 4800 2850 30  0000 C CNN
F 3 "~" H 4850 3050 30  0000 C CNN
	1    4850 3050
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-computer R21
U 1 1 584D9DEF
P 4950 3050
F 0 "R21" V 4900 3250 40  0000 C CNN
F 1 "R" V 4957 3051 40  0000 C CNN
F 2 "100" V 4900 2850 30  0000 C CNN
F 3 "~" H 4950 3050 30  0000 C CNN
	1    4950 3050
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-computer R23
U 1 1 584D9DF5
P 5050 3050
F 0 "R23" V 5000 3250 40  0000 C CNN
F 1 "R" V 5057 3051 40  0000 C CNN
F 2 "100" V 5000 2850 30  0000 C CNN
F 3 "~" H 5050 3050 30  0000 C CNN
	1    5050 3050
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-computer R24
U 1 1 584D9DFB
P 5150 3050
F 0 "R24" V 5100 3250 40  0000 C CNN
F 1 "R" V 5157 3051 40  0000 C CNN
F 2 "100" V 5100 2850 30  0000 C CNN
F 3 "~" H 5150 3050 30  0000 C CNN
	1    5150 3050
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-computer R25
U 1 1 584D9E01
P 5250 3050
F 0 "R25" V 5200 3250 40  0000 C CNN
F 1 "R" V 5257 3051 40  0000 C CNN
F 2 "100" V 5200 2850 30  0000 C CNN
F 3 "~" H 5250 3050 30  0000 C CNN
	1    5250 3050
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-computer R26
U 1 1 584D9E07
P 5350 3050
F 0 "R26" V 5300 3250 40  0000 C CNN
F 1 "R" V 5357 3051 40  0000 C CNN
F 2 "100" V 5300 2850 30  0000 C CNN
F 3 "~" H 5350 3050 30  0000 C CNN
	1    5350 3050
	1    0    0    -1  
$EndComp
$Comp
L R-RESCUE-computer R35
U 1 1 584D9E21
P 9100 2200
F 0 "R35" V 9150 2450 40  0000 C CNN
F 1 "R" V 9107 2201 40  0000 C CNN
F 2 "68" V 9150 2000 30  0000 C CNN
F 3 "~" H 9100 2200 30  0000 C CNN
	1    9100 2200
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-computer R34
U 1 1 584D9E27
P 9100 2100
F 0 "R34" V 9150 2350 40  0000 C CNN
F 1 "R" V 9107 2101 40  0000 C CNN
F 2 "68" V 9150 1900 30  0000 C CNN
F 3 "~" H 9100 2100 30  0000 C CNN
	1    9100 2100
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-computer R33
U 1 1 584D9E37
P 9100 2000
F 0 "R33" V 9150 2250 40  0000 C CNN
F 1 "R" V 9107 2001 40  0000 C CNN
F 2 "68" V 9150 1800 30  0000 C CNN
F 3 "~" H 9100 2000 30  0000 C CNN
	1    9100 2000
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-computer R32
U 1 1 584D9E3D
P 9100 1900
F 0 "R32" V 9150 2150 40  0000 C CNN
F 1 "R" V 9107 1901 40  0000 C CNN
F 2 "68" V 9150 1700 30  0000 C CNN
F 3 "~" H 9100 1900 30  0000 C CNN
	1    9100 1900
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-computer R31
U 1 1 584D9E43
P 9100 1800
F 0 "R31" V 9150 2050 40  0000 C CNN
F 1 "R" V 9107 1801 40  0000 C CNN
F 2 "68" V 9150 1600 30  0000 C CNN
F 3 "~" H 9100 1800 30  0000 C CNN
	1    9100 1800
	0    -1   -1   0   
$EndComp
$Comp
L R-RESCUE-computer R30
U 1 1 584D9E49
P 9100 1700
F 0 "R30" V 9150 1950 40  0000 C CNN
F 1 "R" V 9107 1701 40  0000 C CNN
F 2 "68" V 9150 1500 30  0000 C CNN
F 3 "~" H 9100 1700 30  0000 C CNN
	1    9100 1700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	10650 3150 10800 3150
Wire Wire Line
	10800 3150 10850 3150
Wire Wire Line
	10850 3150 10850 3200
Wire Wire Line
	9950 1250 10050 1250
Wire Wire Line
	10050 1250 10150 1250
Wire Wire Line
	10050 1350 10150 1350
Wire Wire Line
	10050 1450 10150 1450
Wire Wire Line
	10150 1550 10050 1550
Wire Wire Line
	10150 1650 10050 1650
Wire Wire Line
	10150 1750 10050 1750
Wire Wire Line
	10150 1850 10050 1850
Wire Wire Line
	10150 1950 10050 1950
Wire Wire Line
	10150 2050 10050 2050
Wire Wire Line
	10150 2150 10050 2150
Wire Wire Line
	9950 1250 9950 1200
Wire Wire Line
	10650 1550 10750 1550
Wire Wire Line
	10650 1450 10750 1450
Wire Wire Line
	10650 1350 10750 1350
Wire Wire Line
	10650 1250 10750 1250
Wire Wire Line
	10650 1950 10750 1950
Wire Wire Line
	10650 1850 10750 1850
Wire Wire Line
	10650 1750 10750 1750
Wire Wire Line
	10650 1650 10750 1650
Wire Wire Line
	10650 2350 10750 2350
Wire Wire Line
	10650 2250 10750 2250
Wire Wire Line
	10650 2150 10750 2150
Wire Wire Line
	10650 2050 10750 2050
Wire Wire Line
	10650 2750 10750 2750
Wire Wire Line
	10650 2650 10750 2650
Wire Wire Line
	10650 2550 10750 2550
Wire Wire Line
	10650 2450 10750 2450
Wire Wire Line
	10150 2250 10050 2250
Wire Wire Line
	10150 2350 10050 2350
Wire Wire Line
	10150 2450 10050 2450
Wire Wire Line
	10150 2550 10050 2550
Wire Wire Line
	10150 2650 10050 2650
Wire Wire Line
	10150 2750 10050 2750
Wire Wire Line
	10150 2850 10050 2850
Wire Wire Line
	10150 2950 10050 2950
Wire Wire Line
	10750 2850 10650 2850
Wire Wire Line
	10650 2950 10800 2950
Wire Wire Line
	10650 3050 10800 3050
Wire Wire Line
	10150 3050 10050 3050
Wire Wire Line
	10150 3150 10050 3150
Wire Wire Line
	10050 1350 10050 1250
Connection ~ 10050 1250
Wire Wire Line
	10800 3050 10800 3150
Connection ~ 10800 3150
Wire Wire Line
	10300 4600 10400 4600
Wire Wire Line
	10300 4850 10400 4850
Wire Wire Line
	10300 5100 10400 5100
Wire Wire Line
	10300 5350 10400 5350
Wire Wire Line
	10300 5600 10400 5600
Wire Wire Line
	10300 5850 10400 5850
Wire Wire Line
	9700 4600 10000 4600
Wire Wire Line
	10000 4850 10000 4700
Wire Wire Line
	10000 4700 9700 4700
Wire Wire Line
	10000 5100 9950 5100
Wire Wire Line
	9950 5100 9950 4800
Wire Wire Line
	9950 4800 9700 4800
Wire Wire Line
	10000 5350 9900 5350
Wire Wire Line
	9900 5350 9900 4900
Wire Wire Line
	9900 4900 9700 4900
Wire Wire Line
	10000 5600 9850 5600
Wire Wire Line
	9850 5600 9850 5000
Wire Wire Line
	9850 5000 9700 5000
Wire Wire Line
	10000 5850 9800 5850
Wire Wire Line
	9800 5850 9800 5100
Wire Wire Line
	9800 5100 9700 5100
Wire Wire Line
	8100 4600 8400 4600
Wire Wire Line
	8100 4950 8100 4700
Wire Wire Line
	8100 4700 8400 4700
Wire Wire Line
	8100 5300 8150 5300
Wire Wire Line
	8150 5300 8150 4800
Wire Wire Line
	8150 4800 8400 4800
Wire Wire Line
	8100 5650 8200 5650
Wire Wire Line
	8200 5650 8200 4900
Wire Wire Line
	8200 4900 8400 4900
Wire Wire Line
	8100 6000 8250 6000
Wire Wire Line
	8250 6000 8250 5000
Wire Wire Line
	8250 5000 8400 5000
Wire Wire Line
	8100 6350 8300 6350
Wire Wire Line
	8300 6350 8300 5100
Wire Wire Line
	8300 5100 8400 5100
Wire Wire Line
	10800 4600 10950 4600
Wire Wire Line
	10950 4400 10950 4600
Wire Wire Line
	10950 4600 10950 4850
Wire Wire Line
	10950 4850 10950 5100
Wire Wire Line
	10950 5100 10950 5350
Wire Wire Line
	10950 5350 10950 5600
Wire Wire Line
	10950 5600 10950 5850
Wire Wire Line
	10950 4850 10800 4850
Connection ~ 10950 4600
Wire Wire Line
	10950 5100 10800 5100
Connection ~ 10950 4850
Wire Wire Line
	10950 5350 10800 5350
Connection ~ 10950 5100
Wire Wire Line
	10950 5600 10800 5600
Connection ~ 10950 5350
Wire Wire Line
	10950 5850 10800 5850
Connection ~ 10950 5600
Wire Wire Line
	7100 4600 7200 4600
Wire Wire Line
	7100 4950 7200 4950
Wire Wire Line
	7100 5300 7200 5300
Wire Wire Line
	7100 5650 7200 5650
Wire Wire Line
	7100 6000 7200 6000
Wire Wire Line
	7100 6350 7200 6350
Wire Wire Line
	2850 1150 2950 1150
Wire Wire Line
	2950 1150 2950 2400
Wire Wire Line
	2950 2400 1450 2400
Wire Wire Line
	1450 2400 1450 2550
Wire Wire Line
	2850 2650 2950 2650
Wire Wire Line
	2950 2650 2950 3900
Wire Wire Line
	2950 3900 1450 3900
Wire Wire Line
	1450 3900 1450 4050
Wire Wire Line
	1450 2000 1300 2000
Wire Wire Line
	1300 2000 1300 3500
Wire Wire Line
	1300 3500 1300 5000
Wire Wire Line
	1300 5000 1300 5500
Wire Wire Line
	1300 3500 1450 3500
Wire Wire Line
	1300 5000 1450 5000
Connection ~ 1300 3500
Wire Wire Line
	1450 2150 1200 2150
Wire Wire Line
	1200 2150 1200 3650
Wire Wire Line
	1200 3650 1200 5150
Wire Wire Line
	1200 3650 1450 3650
Wire Wire Line
	1200 5150 1400 5150
Wire Wire Line
	1400 5150 1450 5150
Connection ~ 1200 3650
Wire Wire Line
	1450 2250 1100 2250
Wire Wire Line
	1100 2250 1100 3750
Wire Wire Line
	1100 3750 1100 5250
Wire Wire Line
	1100 5250 1100 5400
Wire Wire Line
	1100 3750 1450 3750
Wire Wire Line
	1100 5250 1450 5250
Connection ~ 1100 3750
Wire Wire Line
	900  1150 1450 1150
Wire Wire Line
	900  1250 1450 1250
Wire Wire Line
	900  1350 1450 1350
Wire Wire Line
	900  1450 1450 1450
Wire Wire Line
	900  1550 1450 1550
Wire Wire Line
	900  1650 1450 1650
Wire Wire Line
	900  1750 1450 1750
Wire Wire Line
	900  1850 1450 1850
Wire Wire Line
	900  2650 1450 2650
Wire Wire Line
	900  2750 1450 2750
Wire Wire Line
	900  2850 1450 2850
Wire Wire Line
	900  2950 1450 2950
Wire Wire Line
	900  3050 1450 3050
Wire Wire Line
	900  3150 1450 3150
Wire Wire Line
	900  3250 1450 3250
Wire Wire Line
	900  3350 1450 3350
Wire Wire Line
	900  4150 1450 4150
Wire Wire Line
	900  4250 1450 4250
Wire Wire Line
	900  4350 1450 4350
Wire Wire Line
	900  4450 1450 4450
Wire Wire Line
	900  4550 1450 4550
Wire Wire Line
	900  4650 1450 4650
Wire Wire Line
	900  4750 1450 4750
Wire Wire Line
	900  4850 1450 4850
Connection ~ 1100 5250
Wire Wire Line
	2850 4150 5300 4150
Wire Wire Line
	5300 4150 5300 4850
Wire Wire Line
	5300 4850 5250 4850
Wire Wire Line
	1400 5150 1400 5400
Wire Wire Line
	1400 5400 3000 5400
Wire Wire Line
	3000 5400 3000 4250
Wire Wire Line
	3000 4250 5400 4250
Wire Wire Line
	5400 4250 5400 4950
Wire Wire Line
	5400 4950 5250 4950
Connection ~ 1400 5150
Wire Wire Line
	1300 5500 3100 5500
Wire Wire Line
	3100 5500 3100 4350
Wire Wire Line
	3100 4350 5500 4350
Wire Wire Line
	5500 4350 5500 5050
Wire Wire Line
	5500 5050 5250 5050
Connection ~ 1300 5000
Wire Wire Line
	5250 6250 5800 6250
Wire Wire Line
	5250 6350 5800 6350
Wire Wire Line
	5250 6450 5800 6450
Wire Wire Line
	5250 6550 5800 6550
Wire Wire Line
	5250 6650 5800 6650
Wire Wire Line
	5250 6750 5800 6750
Wire Wire Line
	5250 6850 5800 6850
Wire Wire Line
	5250 5550 5550 5550
Wire Wire Line
	5250 5650 5550 5650
Wire Wire Line
	5250 5750 5550 5750
Wire Wire Line
	5250 5850 5550 5850
Wire Wire Line
	5250 5950 5550 5950
Wire Wire Line
	5250 6050 5550 6050
Wire Bus Line
	5650 3600 5650 5450
Wire Bus Line
	5650 5450 5650 5550
Wire Bus Line
	5650 5550 5650 5650
Wire Bus Line
	5650 5650 5650 5750
Wire Bus Line
	5650 5750 5650 5850
Wire Bus Line
	5650 5850 5650 5950
Wire Bus Line
	3650 3600 5650 3600
Wire Bus Line
	3650 800  3650 3600
Wire Wire Line
	6450 5250 6500 5250
Wire Wire Line
	6500 4950 6500 5200
Wire Wire Line
	6500 5250 6500 5350
Connection ~ 6500 5250
Wire Wire Line
	6100 5150 5250 5150
Wire Wire Line
	5250 5250 6000 5250
Wire Wire Line
	6000 5250 6150 5250
Wire Wire Line
	5250 5350 5800 5350
Wire Wire Line
	5800 5350 5950 5350
Wire Wire Line
	5400 6950 5250 6950
Wire Wire Line
	4250 7550 4250 7450
Wire Wire Line
	6100 3900 6100 4950
Wire Wire Line
	6100 4950 6100 5150
Wire Wire Line
	6100 4950 6150 4950
Wire Wire Line
	6450 4950 6500 4950
Wire Wire Line
	1150 6350 1150 6450
Wire Wire Line
	1150 6450 1250 6450
Wire Wire Line
	1150 6750 1150 6650
Wire Wire Line
	1150 6650 1250 6650
Wire Wire Line
	2200 6550 1850 6550
Wire Wire Line
	7100 3550 7400 3550
Wire Wire Line
	7100 3650 7500 3650
Wire Wire Line
	7100 3750 7400 3750
Wire Wire Line
	6000 3550 6600 3550
Wire Wire Line
	5800 3650 6600 3650
Wire Wire Line
	3350 3750 6600 3750
Wire Wire Line
	7400 3750 7400 3950
Wire Wire Line
	7400 3550 7400 3500
Connection ~ 6100 4950
Wire Wire Line
	6100 3900 7500 3900
Wire Wire Line
	7500 3900 7500 3650
Wire Wire Line
	6000 5250 6000 3550
Connection ~ 6000 5250
Wire Wire Line
	5800 5350 5800 3650
Connection ~ 5800 5350
Wire Wire Line
	3350 4850 3350 3750
Wire Bus Line
	9450 1600 9450 1700
Wire Bus Line
	9450 1700 9450 1800
Wire Bus Line
	9450 1800 9450 1900
Wire Bus Line
	9450 1900 9450 2000
Wire Bus Line
	9450 2000 9450 2100
Wire Bus Line
	9450 2100 9450 2200
Wire Bus Line
	9450 2200 9450 3000
Wire Bus Line
	9450 3000 9450 3400
Wire Bus Line
	9450 3400 9450 3500
Wire Bus Line
	9450 3500 9450 3600
Wire Bus Line
	9450 3600 9450 3700
Wire Bus Line
	9450 3700 9450 3800
Wire Bus Line
	9450 3800 9450 3900
Wire Bus Line
	5900 5700 5900 6150
Wire Bus Line
	5900 6150 5900 6250
Wire Bus Line
	5900 6250 5900 6350
Wire Bus Line
	5900 6350 5900 6450
Wire Bus Line
	5900 6450 5900 6550
Wire Bus Line
	5900 6550 5900 6650
Wire Bus Line
	5900 6650 5900 6750
Wire Bus Line
	5900 5700 6700 5700
Wire Bus Line
	6700 5700 6700 4300
Wire Bus Line
	6700 4300 7950 4300
Wire Bus Line
	7950 4300 7950 3900
Wire Bus Line
	7950 3900 7950 3800
Wire Bus Line
	7950 3800 7950 3700
Wire Bus Line
	7950 3700 7950 3600
Wire Bus Line
	7950 3600 7950 3500
Wire Bus Line
	7950 3500 7950 3400
Wire Wire Line
	6500 1300 6500 1600
Wire Wire Line
	8200 1300 8200 1600
Wire Wire Line
	4350 1400 4350 1300
Wire Wire Line
	4850 1400 4850 1300
Wire Wire Line
	5350 1400 5350 1300
Wire Wire Line
	5850 1400 5850 1300
Wire Wire Line
	8200 2300 8200 2500
Wire Wire Line
	8200 2500 6500 2500
Wire Wire Line
	6500 2500 6500 2300
Wire Wire Line
	6500 2200 6450 2200
Wire Wire Line
	6450 2200 6450 2550
Wire Wire Line
	6450 2550 8250 2550
Wire Wire Line
	8250 2550 8250 2200
Wire Wire Line
	8200 2200 8250 2200
Wire Wire Line
	8250 2200 8850 2200
Wire Wire Line
	6500 2100 6400 2100
Wire Wire Line
	6400 2100 6400 2600
Wire Wire Line
	6400 2600 8300 2600
Wire Wire Line
	8300 2600 8300 2100
Wire Wire Line
	8200 2100 8300 2100
Wire Wire Line
	8300 2100 8850 2100
Wire Wire Line
	6500 2000 6350 2000
Wire Wire Line
	6350 2000 6350 2650
Wire Wire Line
	6350 2650 8350 2650
Wire Wire Line
	8350 2650 8350 2000
Wire Wire Line
	8200 2000 8350 2000
Wire Wire Line
	8350 2000 8850 2000
Wire Wire Line
	6500 1900 6300 1900
Wire Wire Line
	6300 1900 6300 2700
Wire Wire Line
	6300 2700 8400 2700
Wire Wire Line
	8400 2700 8400 1900
Wire Wire Line
	8200 1900 8400 1900
Wire Wire Line
	8400 1900 8850 1900
Wire Wire Line
	6500 1800 6250 1800
Wire Wire Line
	6250 1800 6250 2750
Wire Wire Line
	6250 2750 8450 2750
Wire Wire Line
	8450 2750 8450 1800
Wire Wire Line
	8200 1800 8450 1800
Wire Wire Line
	8450 1800 8850 1800
Wire Wire Line
	6500 1700 6200 1700
Wire Wire Line
	6200 1700 6200 2800
Wire Wire Line
	6200 2800 8500 2800
Wire Wire Line
	8500 2800 8500 1700
Wire Wire Line
	8200 1700 8500 1700
Wire Wire Line
	8500 1700 8850 1700
Connection ~ 8500 1700
Connection ~ 8450 1800
Connection ~ 8400 1900
Connection ~ 8350 2000
Connection ~ 8300 2100
Connection ~ 8250 2200
Wire Wire Line
	8200 2300 8850 2300
Entry Wire Line
	9350 1700 9450 1600
Entry Wire Line
	9350 1800 9450 1700
Entry Wire Line
	9350 1900 9450 1800
Entry Wire Line
	9350 2000 9450 1900
Entry Wire Line
	9350 2100 9450 2000
Entry Wire Line
	9350 2200 9450 2100
Entry Wire Line
	9350 2300 9450 2200
Entry Bus Bus
	9350 3100 9450 3000
Wire Bus Line
	5600 3100 9350 3100
Wire Bus Line
	5600 3400 5600 3100
Wire Bus Line
	5600 3400 5450 3400
Wire Bus Line
	5450 3400 5350 3400
Wire Bus Line
	5350 3400 5250 3400
Wire Bus Line
	5250 3400 5150 3400
Wire Bus Line
	5150 3400 5050 3400
Wire Bus Line
	5050 3400 4950 3400
Wire Bus Line
	4950 3400 4850 3400
Text Label 5300 6250 0    60   ~ 0
a
Text Label 5300 6350 0    60   ~ 0
b
Text Label 5300 6450 0    60   ~ 0
c
Text Label 5300 6550 0    60   ~ 0
d
Text Label 5300 6650 0    60   ~ 0
e
Text Label 5300 6750 0    60   ~ 0
f
Text Label 5300 6850 0    60   ~ 0
g
Text Label 4350 1400 0    60   ~ 0
A1
Text Label 4700 1400 0    60   ~ 0
A2
Text Label 5350 1400 0    60   ~ 0
A3
Text Label 5850 1400 0    60   ~ 0
A4
Text Label 6500 1400 0    60   ~ 0
A5
Text Label 8200 1400 0    60   ~ 0
A6
Text Label 5300 5550 0    60   ~ 0
A1
Text Label 5300 5650 0    60   ~ 0
A2
Text Label 5300 5750 0    60   ~ 0
A3
Text Label 5300 5850 0    60   ~ 0
A4
Text Label 5300 5950 0    60   ~ 0
A5
Text Label 5300 6050 0    60   ~ 0
A6
Text Label 5300 4800 1    60   ~ 0
Q7
Text Label 5250 4950 0    60   ~ 0
CP
Text Label 5250 5050 0    60   ~ 0
PL
NoConn ~ 10050 2250
$Comp
L 74HC04 U21
U 1 1 584DBF57
P 7650 4600
F 0 "U21" H 7800 4700 40  0000 C CNN
F 1 "74HC04" H 7850 4500 40  0000 C CNN
F 2 "~" H 7650 4600 60  0000 C CNN
F 3 "~" H 7650 4600 60  0000 C CNN
	1    7650 4600
	1    0    0    -1  
$EndComp
$Comp
L 74HC04 U21
U 2 1 584DBF64
P 7650 4950
F 0 "U21" H 7800 5050 40  0000 C CNN
F 1 "74HC04" H 7850 4850 40  0000 C CNN
F 2 "~" H 7650 4950 60  0000 C CNN
F 3 "~" H 7650 4950 60  0000 C CNN
	2    7650 4950
	1    0    0    -1  
$EndComp
$Comp
L 74HC04 U21
U 3 1 584DBF6A
P 7650 5300
F 0 "U21" H 7800 5400 40  0000 C CNN
F 1 "74HC04" H 7850 5200 40  0000 C CNN
F 2 "~" H 7650 5300 60  0000 C CNN
F 3 "~" H 7650 5300 60  0000 C CNN
	3    7650 5300
	1    0    0    -1  
$EndComp
$Comp
L 74HC04 U21
U 4 1 584DBF70
P 7650 5650
F 0 "U21" H 7800 5750 40  0000 C CNN
F 1 "74HC04" H 7850 5550 40  0000 C CNN
F 2 "~" H 7650 5650 60  0000 C CNN
F 3 "~" H 7650 5650 60  0000 C CNN
	4    7650 5650
	1    0    0    -1  
$EndComp
$Comp
L 74HC04 U21
U 5 1 584DBF76
P 7650 6000
F 0 "U21" H 7800 6100 40  0000 C CNN
F 1 "74HC04" H 7850 5900 40  0000 C CNN
F 2 "~" H 7650 6000 60  0000 C CNN
F 3 "~" H 7650 6000 60  0000 C CNN
	5    7650 6000
	1    0    0    -1  
$EndComp
$Comp
L 74HC04 U21
U 6 1 584DBF7C
P 7650 6350
F 0 "U21" H 7800 6450 40  0000 C CNN
F 1 "74HC04" H 7850 6250 40  0000 C CNN
F 2 "~" H 7650 6350 60  0000 C CNN
F 3 "~" H 7650 6350 60  0000 C CNN
	6    7650 6350
	1    0    0    -1  
$EndComp
$Comp
L R_Small R17
U 1 1 585075DE
P 4050 1000
F 0 "R17" H 4080 1020 50  0000 L CNN
F 1 "R" H 4080 960 50  0000 L CNN
F 2 "1k" H 3950 950 50  0000 C CNN
F 3 "" H 4050 1000 50  0000 C CNN
	1    4050 1000
	-1   0    0    1   
$EndComp
$Comp
L MMBT3904 Q3
U 1 1 585076E1
P 4250 1100
F 0 "Q3" H 4000 1050 50  0000 L CNN
F 1 "2N3904" H 3950 950 50  0000 L CNN
F 2 "~" H 4450 1025 50  0000 L CIN
F 3 "" H 4250 1100 50  0000 L CNN
	1    4250 1100
	1    0    0    -1  
$EndComp
$Comp
L VCC #PWR64
U 1 1 585085D6
P 4350 650
F 0 "#PWR64" H 4350 500 50  0001 C CNN
F 1 "VCC" H 4350 800 50  0000 C CNN
F 2 "" H 4350 650 50  0000 C CNN
F 3 "" H 4350 650 50  0000 C CNN
	1    4350 650 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 650  4350 700 
Wire Wire Line
	4350 700  4350 900 
$Comp
L R_Small R18
U 1 1 58508864
P 4550 1000
F 0 "R18" H 4580 1020 50  0000 L CNN
F 1 "R" H 4580 960 50  0000 L CNN
F 2 "1k" H 4450 950 50  0000 C CNN
F 3 "" H 4550 1000 50  0000 C CNN
	1    4550 1000
	-1   0    0    1   
$EndComp
$Comp
L MMBT3904 Q4
U 1 1 5850886A
P 4750 1100
F 0 "Q4" H 4500 1050 50  0000 L CNN
F 1 "2N3904" H 4450 950 50  0000 L CNN
F 2 "~" H 4950 1025 50  0000 L CIN
F 3 "" H 4750 1100 50  0000 L CNN
	1    4750 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 900  4850 700 
Connection ~ 4350 700 
Entry Wire Line
	4950 800  5050 900 
$Comp
L R_Small R22
U 1 1 585091C5
P 5050 1000
F 0 "R22" H 5080 1020 50  0000 L CNN
F 1 "R" H 5080 960 50  0000 L CNN
F 2 "1k" H 4950 950 50  0000 C CNN
F 3 "" H 5050 1000 50  0000 C CNN
	1    5050 1000
	-1   0    0    1   
$EndComp
$Comp
L MMBT3904 Q5
U 1 1 585091CB
P 5250 1100
F 0 "Q5" H 5000 1050 50  0000 L CNN
F 1 "2N3904" H 4950 950 50  0000 L CNN
F 2 "~" H 5450 1025 50  0000 L CIN
F 3 "" H 5250 1100 50  0000 L CNN
	1    5250 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 700  5350 900 
Entry Wire Line
	5450 800  5550 900 
$Comp
L R_Small R27
U 1 1 585094F6
P 5550 1000
F 0 "R27" H 5580 1020 50  0000 L CNN
F 1 "R" H 5580 960 50  0000 L CNN
F 2 "1k" H 5450 950 50  0000 C CNN
F 3 "" H 5550 1000 50  0000 C CNN
	1    5550 1000
	-1   0    0    1   
$EndComp
$Comp
L MMBT3904 Q6
U 1 1 585094FC
P 5750 1100
F 0 "Q6" H 5500 1050 50  0000 L CNN
F 1 "2N3904" H 5450 950 50  0000 L CNN
F 2 "~" H 5950 1025 50  0000 L CIN
F 3 "" H 5750 1100 50  0000 L CNN
	1    5750 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	5850 700  5850 900 
Wire Wire Line
	4350 700  4850 700 
Wire Wire Line
	4850 700  5350 700 
Wire Wire Line
	5350 700  5850 700 
Wire Wire Line
	5850 700  6500 700 
Wire Wire Line
	6500 700  8200 700 
Connection ~ 4850 700 
Connection ~ 5350 700 
Entry Wire Line
	6100 800  6200 900 
$Comp
L R_Small R28
U 1 1 5850A992
P 6200 1000
F 0 "R28" H 6230 1020 50  0000 L CNN
F 1 "R" H 6230 960 50  0000 L CNN
F 2 "1k" H 6100 950 50  0000 C CNN
F 3 "" H 6200 1000 50  0000 C CNN
	1    6200 1000
	-1   0    0    1   
$EndComp
$Comp
L MMBT3904 Q7
U 1 1 5850A998
P 6400 1100
F 0 "Q7" H 6150 1050 50  0000 L CNN
F 1 "2N3904" H 6100 950 50  0000 L CNN
F 2 "~" H 6600 1025 50  0000 L CIN
F 3 "" H 6400 1100 50  0000 L CNN
	1    6400 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 700  6500 900 
Entry Wire Line
	7800 800  7900 900 
$Comp
L R_Small R29
U 1 1 5850B21C
P 7900 1000
F 0 "R29" H 7930 1020 50  0000 L CNN
F 1 "R" H 7930 960 50  0000 L CNN
F 2 "1k" H 7800 950 50  0000 C CNN
F 3 "" H 7900 1000 50  0000 C CNN
	1    7900 1000
	-1   0    0    1   
$EndComp
$Comp
L MMBT3904 Q8
U 1 1 5850B222
P 8100 1100
F 0 "Q8" H 7850 1050 50  0000 L CNN
F 1 "2N3904" H 7800 950 50  0000 L CNN
F 2 "~" H 8300 1025 50  0000 L CIN
F 3 "" H 8100 1100 50  0000 L CNN
	1    8100 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 700  8200 900 
Connection ~ 5850 700 
Connection ~ 6500 700 
Wire Bus Line
	3650 800  3950 800 
Wire Bus Line
	3950 800  4450 800 
Wire Bus Line
	4450 800  4950 800 
Wire Bus Line
	4950 800  5450 800 
Wire Bus Line
	5450 800  6100 800 
Wire Bus Line
	6100 800  7800 800 
$Comp
L R R37
U 1 1 58512A1B
P 10150 4600
F 0 "R37" V 10230 4600 50  0000 C CNN
F 1 "R" V 10150 4600 50  0000 C CNN
F 2 "220" V 10080 4600 50  0000 C CNN
F 3 "" H 10150 4600 50  0000 C CNN
	1    10150 4600
	0    1    1    0   
$EndComp
$Comp
L R R38
U 1 1 58512B83
P 10150 4850
F 0 "R38" V 10230 4850 50  0000 C CNN
F 1 "R" V 10150 4850 50  0000 C CNN
F 2 "220" V 10080 4850 50  0000 C CNN
F 3 "" H 10150 4850 50  0000 C CNN
	1    10150 4850
	0    1    1    0   
$EndComp
$Comp
L R R39
U 1 1 58512C38
P 10150 5100
F 0 "R39" V 10230 5100 50  0000 C CNN
F 1 "R" V 10150 5100 50  0000 C CNN
F 2 "220" V 10080 5100 50  0000 C CNN
F 3 "" H 10150 5100 50  0000 C CNN
	1    10150 5100
	0    1    1    0   
$EndComp
$Comp
L R R40
U 1 1 58512CF8
P 10150 5350
F 0 "R40" V 10230 5350 50  0000 C CNN
F 1 "R" V 10150 5350 50  0000 C CNN
F 2 "220" V 10080 5350 50  0000 C CNN
F 3 "" H 10150 5350 50  0000 C CNN
	1    10150 5350
	0    1    1    0   
$EndComp
$Comp
L R R41
U 1 1 58512DB7
P 10150 5600
F 0 "R41" V 10230 5600 50  0000 C CNN
F 1 "R" V 10150 5600 50  0000 C CNN
F 2 "220" V 10080 5600 50  0000 C CNN
F 3 "" H 10150 5600 50  0000 C CNN
	1    10150 5600
	0    1    1    0   
$EndComp
$Comp
L R R42
U 1 1 58512E75
P 10150 5850
F 0 "R42" V 10230 5850 50  0000 C CNN
F 1 "R" V 10150 5850 50  0000 C CNN
F 2 "220" V 10080 5850 50  0000 C CNN
F 3 "" H 10150 5850 50  0000 C CNN
	1    10150 5850
	0    1    1    0   
$EndComp
$EndSCHEMATC
